#pragma warning ( disable : 4996)
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define MAX INT_MIN

struct sp {
	int value;
	struct sp *next;
} *F, *F2, *F3;
//добавление первого эл-та
int addFirst(struct sp **first, int value) {
	struct sp *tmp;
	tmp = (struct sp*)malloc(sizeof(struct sp));
	if (tmp == 0)
		return 0;
	tmp->value = value;
	tmp->next = NULL;
	if (*first == NULL)/*no elements*/
	{
		*first = tmp;
		(*first)->next = NULL;
		return 1;
	}
	tmp->next = *first;
	*first = tmp;
	return 1;
};
// добавление последнего эл-та
void addLast(struct sp **first, int value) {
	struct sp *tmp, *current;
	tmp = (struct sp*)malloc(sizeof(struct sp));
	tmp->value = value;
	tmp->next = NULL;
	if (*first == NULL)/*no elements*/
	{
		*first = tmp;
		return;
	}
	current = *first;
	while (current->next != NULL) {
		current = current->next;
	}
	current->next = tmp;
	return;
}

// вычисление размера списка
int size(struct sp *current) {
	if (current == NULL)
	{
		return 0;
	}
	return 1 + size(current->next);
}
//поиск максимального элемента
int search_max(struct sp *current, int max)
{
	if (current == NULL)
	{
		return max;
	}
	if (current->value > max)
	{
		max = current->value;
	}
	return search_max(current->next, max);
}

//поиск элемента
int search(struct sp *current, int value) {
	if (current == NULL)
	{
		return 0;
	}
	if (current->value == value)
		return 1 + search(current->next, value);
	else
		return search(current->next, value);
}

//Кол-во эл-тов с заданным значением
int countValue(struct sp **first, int value) {
	struct sp *current;
	int count = 0;
	current = *first;
	while (current != NULL) {
		if (current == value)
			count++;
		current = current->next;
	}
	printf("%d", count);
}

void removevalue(struct sp **head,  int value)
{
	struct sp *current, *prev;
	current = *head;
	while (current ->next-> value != value) {
		current = current->next;
	}
	prev = current->next ;
	current -> next = prev->next;
	free(prev);
	return ;
}
// удаление первого элемента

int removeFirst(struct sp **first) {
	struct sp* prev = NULL;
	if (first == NULL) {
		return 0;
	}
	prev = (*first);
	(*first) = (*first)->next;
	free(prev);
	return *first;
}

//максимальный первый эл-т или нет
int maxfirstorno(struct sp *head, int max) {
	if (head->value == max)
		return 1;
	else
		return 0;
}

//удаление максимальных элементов 
/* Сам указатель на указатель содержит в себе адрес, который ссылается на другой адрес,
а он, в свою очередь,  ссылается на адрес в памяти, где хранятся данные*/
int remove_MAX(struct sp **first, int max)
{
	struct sp *current, *prev;
	current = *first;
	if (first == NULL)
	{
		return 0;
	}
	while (current->next->value != max) {
		current = current->next;
	}
	prev = current->next;
	current->next = prev->next;
	free(prev);
	return remove_MAX(current, max);
 }
//чистит список 
void freeList(struct sp **first) {
	while (*first != NULL)/*memory free*/
	{
		struct sp *h;
		h = *first;
		*first = (*first)->next;
		free(h);
	}
	*first = NULL;
	return;
};

void printList(struct sp *head) {
	while (head) {
		printf("%d ", head->value);
		head = head->next;
	}
	printf("\n");
}

//удаляет первый максимальный элемент
int deletefirstMax()
{
	int max = F->value;
	struct sp *N, *prev, *T;
	N = F;
	prev = F;
	while (N->next)
	{
		if (N->value > max)
			max = N->value;
		N = N->next;
	}
	if (N->value > max)
		max = N->value;
	if (F->value == max)
	{
		prev = F;
		F = F->next;
		free(prev);
		return;
	}
	while (prev->next->value != max)
	{
		prev = prev->next;
	}
	T = prev->next;
	prev->next = T->next;
	free(T);
	//max = F->next->value;
	return 1;
}

//удаляет все максимальные элементы
int deleteAllMax()
{
	int max = F->value, count=0, i, count2=0;
	struct sp *prev, *sup, *p, *pp;
	sup = F;
	pp = F;
	while (sup->next)
	{
		if (sup->value > max)
			max = sup->value;
		sup = sup->next;
	}
	if (sup->value > max)
		max = sup->value;
	//поиск максимальных чисел
	sup = F;
	while (sup->next)
	{
		if (sup->value == max)
			count++;
		sup = sup->next;
	}
	if (sup->value == max)
		count++;
	count2 = count;
	for (i = 0; i < count; i++)
	{
		if (F->value == max)
		{
			prev = F;
			F = F->next;
			count2--;
			free(prev);
		}
	}
	pp = F;
	for (i = 0; i < count2; i++)
	{
		while (pp->next->value != max)
		{
			pp = pp->next;
		}
		p = pp->next;
		pp->next = p->next;
		free(p);
	}
}

//удаляет все четные
int deleteAllchet()
{
	struct sp *sup, *prev, *pp;
	int count = 0, count2 = 0, i;
	sup = F;
	while (sup->next)
	{
		if (sup->value % 2 == 0)
			count++;
		sup = sup->next;
	}
	if (sup->value % 2 == 0)
		count++;
	count2 = count;
	for (i = 0; i < count; i++)
	{
		if (F->value % 2 == 0)
		{
			prev = F;
			F = F->next;
			count2--;
			free(prev);
		}
	}
	pp = F;
	for (i = 0; i < count2; i++)
	{
		while (pp->next->value % 2 != 0)
		{
			pp = pp->next;
		}
		prev = pp->next;
		pp->next = prev->next;
		free(prev);
	}
}

//удаляет все нечетные
void deleteAllnechet()
{
	struct sp *sup, *prev, *pp;
	int count = 0, count2 = 0, i;
	sup = F;
	while (sup->next)
	{
		if (sup->value % 2 != 0)
			count++;
		sup = sup->next;
	}
	if (sup->value % 2 != 0)
		count++;
	count2 = count;
	for (i = 0; i < count; i++)
	{
		if (F->value % 2 != 0)
		{
			prev = F;
			F = F->next;
			count2--;
			free(prev);
		}
	}
	pp = F;
	for (i = 0; i < count2; i++)
	{
		while (pp->next->value % 2 == 0)
		{
			pp = pp->next;
		}
		prev = pp->next;
		pp->next = prev->next;
		free(prev);
	}
}

//удаляет все числа, которые не входят в интервал
int deleteNoInterval(int a, int b)
{
	int count = 0, count2 = 0, i;
	struct sp *prev, *sup;
	sup = F;
	while (sup->next)
	{
		if (sup->value<a || sup->value>b)
			count++;
		sup = sup->next;
	}
	if (sup->value<a || sup->value>b)
		count++;
	count2 = count;
	for (i = 0; i < count; i++)
	{
		if (F->value <a || F->value > b)
		{
			prev = F;
			F = F->next;
			count2--;
			free(prev);
		}
	}
	sup = F;
	for (i = 0; i < count2; i++)
	{
		while (sup->next->value >= a && sup->next->value <= b)
			sup = sup->next;
		prev = sup->next;
		sup->next = prev->next;
		free(prev);
	}
	return count;
}

//переместить первое максимальное число в конец
void fMaxinEnd()
{
	struct sp *sup, *prev, *tmp=NULL;
	int max=F->value;
	sup = F;
	while (sup->next)
	{
		if (sup->value > max)
			max = sup->value;
		sup = sup->next;
	}
	if (sup->value > max)
		max = sup->value;
	sup = F;
	if (F->value == max)
	{
		tmp = (struct sp*)malloc(sizeof(struct sp));
		prev = F;
		tmp->value = prev->value;
		tmp->next = NULL;
		F = F->next;
		while (sup->next)
		{
			sup = sup->next;
		}
		sup->next = tmp;
		free(prev);
		return 1;
	}
	sup = F;
	while (sup->next->value != max)
		sup = sup->next;
	tmp = (struct sp*)malloc(sizeof(struct sp));
	prev = sup->next;
	sup->next = prev->next;
	tmp->next = NULL;
	tmp->value = prev->value;
	while (sup->next)
	{
		sup = sup->next;
	}
	sup->next = tmp;
	free(prev);
}

//delete first and last nechet element
int delFaLnech()
{
	struct sp *sup, *prev;
	int count, count2=0, i;
	count = 0;
	sup = F;
	prev = F;
	while (sup->next)
	{
		if (sup->value % 2 != 0)
			count++;
		sup = sup->next;
	}
	if (sup->value % 2 != 0)
		count++;
	if (count < 2)
	{
		printf("Error");
		getch();
		return;
	}
	sup = F;
	if (F->value % 2 != 0)
		count--;
	while (sup->next)
	{
		if (sup->next->value % 2 != 0)
		{
			count--;
			if (count == 0)
			{
				prev = sup->next;
				sup->next = prev->next;
				free(prev);
				break;
			}
		}
		sup = sup->next;
	}

	sup = F;
	while (1)
	{
		if (F->value % 2 != 0)
		{
			prev = F;
			F = F->next;
			free(prev);
			break;
		}
		while (sup->next->value % 2 == 0)
			sup = sup->next;
		prev = sup->next;
		sup->next = prev->next;
		free(prev);
		break;
	}
}

//all MIN in end list
int allMinIeL()
{
	struct sp *sup, *prev, *tmp=NULL, *T;
	int count = 0, count2 = 0, min = F->value;
	sup = F;
	while (sup->next)
	{
		if (sup->value < min)
			min = sup->value;
		sup = sup->next;
	}
	if (sup->value < min)
		min = sup->value;
	//count min el-t
	sup = F;
	while (sup->next)
	{
		if (sup->value == min)
		{
			count++;
		}
		sup = sup->next;
	}
	if (sup->value == min)
	{
		count++;
	}
	while (F->value == min && count)
	{
		count--;
		tmp = (struct sp*)malloc(sizeof(struct sp));
		tmp->value = F->value;
		tmp->next = NULL;
		F = F->next;
		sup = F;
		while (sup->next)
			sup = sup->next;
		sup->next = tmp;
	}
	sup = F;
	//return min;
	while (count)
	{
		sup = F;
		while (sup->next->value != min)
			sup = sup->next;
		count--;
		tmp = (struct sp*)malloc(sizeof(struct sp));
		prev = sup->next;
		sup->next = prev->next;
		tmp->next = NULL;
		tmp->value = prev->value;
		while (sup->next)
		{
			sup = sup->next;
		}
		sup->next = tmp;
		free(prev);
	}
}

//удалить элемент равный сумме своих соседей
int delsosed()
{
	struct sp *sup, *prev, *tmp;
	int i, count = 0, sum = 0;
	while (F->value == F->next->value)
	{
		prev = F;
		F = F->next;
		free(prev);
	}
	sup = F;
	while (sup->next->next)
		sup = sup->next;
	if (sup->value == sup->next->value)
	{
		prev = sup->next;
		sup->next = prev->next;
		free(prev);
	}
	sup = F;
	while (sup->next->next)
	{
		sum = sup->value + sup->next->next->value;
		if (sup->next->value == sum)
		{
			prev = sup->next;
			sup->next = prev->next;
			free(prev);
		}
		sup = sup->next;
		if (!(sup->next))
			break;
	}
}

//преносит меньше среднего арифметического в начало1!! вухуууу веселуха!!!!!!!!
int mSrednAinFirst()
{
	struct sp *sup, *prev, *tmp;
	int count = 1, i, sredar = F->value;
	sup = F;
	while (sup->next)
	{
		sredar = sredar + sup->next->value ;
		sup = sup->next;
		count++;
	}
	sredar = sredar / count;
	sup = F;
	while (sup->next)
	{
		count = 0;
		if (sup->next->value < sredar)
		{
			tmp = (struct sp*)malloc(sizeof(struct sp));
			prev = sup->next;
			tmp->value = sup->next->value;
			tmp->next = F;
			F = tmp;
			sup->next = prev->next;
			free(prev);
			count++;
		}
		if (count == 0)
		sup = sup->next;
		if (!sup)
			break;
	}
	return(sredar);
}

//все отрицательные числа в голову
int minusInHead()
{
	struct sp *sup, *prev, *tmp;
	int count = 0;
	sup = F;
	while (sup->next)
	{
		count = 0;
		if (sup->next->value < 0)
		{
			tmp = (struct sp*)malloc(sizeof(struct sp));
			tmp->next = F;
			tmp->value = sup->next->value;
			prev = sup->next;
			sup->next = prev->next;
			F = tmp;
			free(prev);
			count++;
		}
		if(count == 0)
		sup = sup->next;
		if (!sup)
			break;
	}
}

//Все + числа переместить в конец.
int plusinend()
{
	struct sp *sup, *prev, *tmp;
	int count = 0, count2 = 0;
	sup = F;
	while (sup->next) {
		if (sup->value > 0)
			count2++;
		sup = sup->next;
	}
	if (sup->value > 0)
		count2++;

	while (F->value > 0 && count2)
	{
		count2--;
		tmp = (struct sp*)malloc(sizeof(struct sp));
		tmp->value = F->value;
		tmp->next = NULL;
		F = F->next;
		sup = F;
		while (sup->next)
			sup = sup->next;
		sup->next = tmp;
	}
	while (count2)
	{
		sup = F;
		while (sup->next->value < 0)
			sup = sup->next;
		count2--;
		tmp = (struct sp*)malloc(sizeof(struct sp));
		prev = sup->next;
		sup->next = prev->next;
		tmp->next = NULL;
		tmp->value = prev->value;
		while (sup->next)
		{
			sup = sup->next;
		}
		sup->next = tmp;
		free(prev);
	}
	return(count2);
}

//все четные и + числа переместить в начало
int func13()
{
	struct sp *sup, *prev, *tmp;
	int flag = 0;
	sup = F;
	while (sup->next)
	{
		flag = 0;
		if (sup->next->value % 2 == 0 && sup->next->value > 0)
		{
			tmp = (struct sp*)malloc(sizeof(struct sp));
			tmp->next = F;
			tmp->value = sup->next->value;
			prev = sup->next;
			sup->next = prev->next;
			free(prev);
			F = tmp;
			flag++;
		}
		if (!flag)
			sup = sup->next;
		if (!sup)
			break;
	}
}

//удалить элемент который больше первого
int func14()
{
	struct sp *sup, *prev;
	int flag = 0;
	sup = F;
	while (sup->next)
	{
		flag = 0;
		if (sup->next->value > F->value)
		{
			prev = sup->next;
			sup->next = prev->next;
			flag++;
			//free(prev);
		}
		if (!flag)
			sup = sup->next;
		if (!sup)
			break;
	}
}

//удалить каждый 4 эл-т
int func15()
{
	struct sp *prev, *sup;
	int count = 3;
	sup = F;
	while (sup->next)
	{
		count--;
		if (!count)
		{
			prev = sup->next;
			sup->next = prev->next;
			count = 3;
		}
		sup = sup->next;
		if (!sup)
			break;
	}
}

//Удаляет элемент который после MIN
int func16()
{
	int min = F->value, count=0;
	struct sp *prev, *sup, *tmp;
	sup = F;
	while (sup->next)
	{
		if (sup->value < min)
			min = sup->value;
		sup = sup->next;
	}
	sup = F;
	while (sup->next)
	{
		count = 0;
		if (sup->value == min)
		{
			if (sup->next->value == min)
				count++;
			prev = sup->next;
			sup->next = prev->next;
			free(prev);
		}
		if (!count)
			sup = sup->next;
		if (!sup)
			break;
	}
}

//Поменять местами соседние элементы.
int func17()
{
	struct sp *sup, *tmp, *next, *prev;
	sup = F;
	prev = F;
	while (sup->next)
	{
		tmp = (struct sp*)malloc(sizeof(struct sp));
		tmp->value = sup->next->value;
		sup->next->value = sup->value;
		sup->value = tmp->value;
		sup = sup->next->next;
		if (!sup)
			break;
	}
}

//Удалить элемент стоящий перед MAX.
int func18()
{
	struct sp *sup, *prev;
	int max = F->value;
	sup = F;
	while (sup->next)
	{
		if (sup->value > max)
			max = sup->value;
		sup = sup->next;
	}
	if (sup->value > max)
		max = sup->value;
	while (F->next->value == max)
	{
		prev = F;
		F = F->next;
		free(prev);
		if (!F->next)
		{
			getch();
			return;
		}
	}
	sup = F;
	while (sup->next )
	{
		if (sup->next->next->value == max)
		{
			prev = sup->next;
			sup->next = prev->next;
			free(prev);
		}
		sup = sup->next;
		if (!sup)
			break;
		if (!(sup->next->next))
			break;
	}
}

//удаляет к-ый элемент списка
int func19(int k)
{
	struct sp *sup, *prev;
	int count = 1;
	sup = F;
	while (sup->next)
	{
		++count;
		if (count == k)
		{
			prev = sup->next;
			sup->next = prev->next;
			free(prev);
		}
		sup = sup->next;
		if (!sup)
			break;
	}
}

//Все элементы которые больше
//порядкового номера поместить
//в другой список.
int func20()
{
	struct sp *sup, *prev, *tmp;
	int count = 0, flag = 0;
	sup = F;
	while (sup)
	{
		++count;
		if (sup->value > count)
		{
			flag++;
			tmp = (struct sp*)malloc(sizeof(struct sp));
			tmp->value = sup->value;
			tmp->next = NULL;
			if (F2 == NULL)/*no elements*/
			{
				F2 = tmp;
			}
			if (flag > 1)
			{
				prev = F2;
				while (prev->next != NULL) {
					prev = prev;
				}
				prev->next = tmp;
			}
		}
		sup = sup->next;
		if (!sup)
			break;
	}
	
}

//чередует первый и 2-ой список + делает 3
int func21()
{
	struct sp *prev, *sup, *sup2, *tmp;
	sup = F;
	sup2 = F2;
	while (sup->next )
	{
		tmp = (struct sp*)malloc(sizeof(struct sp));
		tmp->value = sup2->value;
		tmp->next = sup->next;
		sup->next = tmp;
		sup = sup->next->next;
		sup2 = sup2->next;
		if (!sup || !sup2)
			break;
	}
}

//все четные элементы в другой список
int func22()
{
	struct sp *prev, *sup, *tmp;
	int flag = 0;
	sup = F;
	prev = F2;
	while (sup)
	{
		if (sup->value % 2 == 0)
		{
			flag++;
			tmp = (struct sp*)malloc(sizeof(struct sp));
			tmp->value = sup->value;
			tmp->next = NULL;
			if (F2 == NULL)/*no elements*/
			{
				F2 = tmp;
			}
			
			if (flag > 1)
			{
				prev = F2;
				while (prev->next != NULL) {
					prev = prev->next;
				}
				prev->next = tmp;
			}
		}
		sup = sup->next;
		if (!sup)
			break;
	}
}

//отрезает список, который длинней и остаток во второй
int func23()
{
	struct sp *sup, *prev, *tmp, *sup2, *sup3;
	int count1 = 1, count2 = 1, flag = 0;
	sup2 = F2;
	sup = F;
	while (sup->next)
	{
		count1++;
		sup = sup->next;
	}
	while (sup2->next)
	{
		count2++;
		sup2 = sup2->next;
	}
	if (count1 > count2)
	{
		sup = F;
		while (count2)
		{
			sup = sup->next;
			count2--;
		}
		while (sup)
		{
			tmp = (struct sp*)malloc(sizeof(struct sp));
			tmp->value = sup->value;
			tmp->next = NULL;
			if (F3 == NULL)
				F3 = tmp;
			flag++;
			if (flag > 1)
			{
				prev = F3;
				while (prev->next != NULL)
					prev = prev->next;
				prev->next = tmp;
			}
			sup = sup->next;
		}
	}
	if (count2 > count1)
	{
		sup2 = F2;
		while (count1)
		{
			sup2 = sup2->next;
			count1--;
		}
		while (sup2)
		{
			tmp = (struct sp*)malloc(sizeof(struct sp));
			tmp->value = sup2->value;
			tmp->next = NULL;
			if (F3 == NULL)
				F3 = tmp;
			flag++;
			if (flag > 1)
			{
				prev = F3;
				while (prev->next != NULL)
					prev = prev->next;
				prev->next = tmp;
			}
			sup2 = sup2->next;
		}
	}
}

int main(int argc, char** argv) {
	int n, rez = 0, s = 0, i, value, count;
	printf("Razmer spiska:\n");
	rez = scanf("%d", &n);
	if (rez == 0 || n < 1) {
		printf("Razmer spiska dolzhen zelyam polozhitelnim\n");
		return 0;
	}
	printf("Wwedite spisok:\n");
	for (i = 0; i<n; i++) {
		rez = scanf("%d", &value);
		if (rez == 0) {
			printf("Element zeliy dolzhen bit\n");
			return 0;
		}
		addLast(&F, value);
	}
	printf("Razmer spiska:\n");
	rez = scanf("%d", &n);
	if (rez == 0 || n < 1) {
		printf("Razmer spiska dolzhen zelyam polozhitelnim\n");
		return 0;
	}
	printf("Wwedite spisok:\n");
	for (i = 0; i<n; i++) {
		rez = scanf("%d", &value);
		if (rez == 0) {
			printf("Element zeliy dolzhen bit\n");
			return 0;
		}
		addLast(&F2, value);
	}
	rez = func23();
	printList(F);
	printList(F2);
	printList(F3);
	freeList(&F);
	freeList(&F2);
	freeList(&F3);
	printf("%d", rez);
	getch();
	return (EXIT_SUCCESS);
}